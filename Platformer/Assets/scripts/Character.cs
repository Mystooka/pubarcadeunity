﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    protected Joystick joystick;
    protected JoyButton joybutton;
    Animator anim;
    private bool facingRight = true;
    private int life = 8;
    private int spellLVL=0;
    private Rigidbody2D rb;
    private float horMov;
    private float vertMov;
    private float spd = 3.5f;
    public GameObject[] spellSpawners;
    private float someScale;
    // Start is called before the first frame update
    void Start()
    {
        someScale = transform.localScale.x;
        //spellSpawners = GetComponentsInChildren<GameObject>();
        joystick = FindObjectOfType<Joystick>();
        //joybutton = FindObjectOfType<JoyButton>();
        rb = this.GetComponent<Rigidbody2D>();
        //anim = GetComponent<Animator>();
        //anim = GetComponentInParent<Animator>();
        anim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        rb.velocity = new Vector2(joystick.Horizontal * 2.5f, joystick.Vertical * 2.5f);

        if (rb.velocity.x > .1 || rb.velocity.x < -.1 || rb.velocity.y > .1 || rb.velocity.y < -.1)
        {
            if (rb.velocity.x > .1)
            {
                transform.localScale = new Vector2(someScale, transform.localScale.y);


            }
            if (rb.velocity.x < -.1)
            {
                transform.localScale = new Vector2(-someScale, transform.localScale.y);

            }

            anim.SetBool("isWalking", true);

        }
        else
        {
            //anim.SetBool("Walking", false);
            anim.SetBool("isWalking", false);
        }
    }
    public void CastSpell()
    {

        if (spellLVL == 0)
        {
            spellSpawners[spellLVL].transform.GetComponent<PlayerSpellSpawner>().CastSpell();
        }

    }
}
