﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorObjSpawner : MonoBehaviour
{
    public GameObject[] plants;
    public GameObject[] rocks;
    public GameObject[] objects;
    private int ran;
    // Start is called before the first frame update
    void Start()
    {
        ran = Random.Range(1,50);

        if (ran <= 5)
        {
            Instantiate(plants[Random.Range(0, plants.Length)], transform.position, Quaternion.identity);
        }
        else if (ran >= 20 && ran <= 25)
        {
            Instantiate(rocks[Random.Range(0, rocks.Length)], transform.position, Quaternion.identity);
        }
        else if (ran == 9)
        {
            Instantiate(objects[Random.Range(0, objects.Length)], transform.position, Quaternion.identity);
        }
        else{
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
