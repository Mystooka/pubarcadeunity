﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{
    public float speed;

    private Transform target;
    public float stoppingDist;
    public float shootDist;
    private float shotDelay;
    public float startShotTime;

    public GameObject projectile;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        shotDelay = startShotTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(transform.position, target.position) > stoppingDist)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
        else if(Vector2.Distance(transform.position, target.position) < stoppingDist && Vector2.Distance(transform.position, target.position) > shootDist)
        {
            transform.position = this.transform.position;
        }

        if (shotDelay <= 0){
            Instantiate(projectile, transform.position, Quaternion.identity);
            shotDelay = startShotTime;
        }
        else
        {
            shotDelay -= Time.deltaTime;
        }
        
    }
}
