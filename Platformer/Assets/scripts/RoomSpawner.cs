﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSpawner : MonoBehaviour
{
    public int openingDir;
    private RoomTemplate template;
    //1 ==> need btm    
    //2 ==> need top    
    //3 ==> need lft    
    //4 ==> need right
    public bool spawned = false;
    private int rand;
    public float waitTime = 4f;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, waitTime);
        template = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplate>();
        Invoke("Spawn", 0.1f);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("SpawnPoint"))
        {
            if (other.GetComponent<RoomSpawner>().spawned == false && this.spawned == false)
            {
                //spawn walls to block opening  
                Instantiate(template.closedRoom, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
            //Destroy(gameObject);
            spawned = true;
        }
    }

    // Update is called once per frame
    void Spawn()
    {
        if (spawned == false)
        {

            if (openingDir == 1)
            {
                rand = Random.Range(0, template.btmRooms.Length);
                Instantiate(template.btmRooms[rand], transform.position, template.btmRooms[rand].transform.rotation);
                
            }
            else if (openingDir == 2)
            {
                rand = Random.Range(0, template.topRooms.Length);
                Instantiate(template.topRooms[rand], transform.position, template.topRooms[rand].transform.rotation);
            }
            else if (openingDir == 3)
            {
                rand = Random.Range(0, template.lftRooms.Length);
                Instantiate(template.lftRooms[rand], transform.position, template.lftRooms[rand].transform.rotation);

            }
            else if (openingDir == 4)
            {
                rand = Random.Range(0, template.rgtRooms.Length);
                Instantiate(template.rgtRooms[rand], transform.position, template.rgtRooms[rand].transform.rotation);
            }
            spawned = true;


        }



    }

}
