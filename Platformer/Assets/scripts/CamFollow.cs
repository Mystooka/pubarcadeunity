﻿
using UnityEngine;

public class CamFollow : MonoBehaviour
{
    public Transform target;
    public float smoothSpeed = .125f;
    public Vector3 offset;

    void FixedUpdate()
    {
        Vector3 desiredPos = target.position + offset;
        Vector3 smothPos = Vector3.Lerp(target.position, desiredPos, smoothSpeed);
        transform.position = smothPos;

        transform.LookAt(target);

    }
}
