﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellControls : MonoBehaviour
{
    private int spellLVL = 1;
    private float dist;
    private Vector2 target;
    private float speed = 3f;
    Rigidbody2D spellRB;
    // Start is called before the first frame update
    void Start()
    {
        //target = new Vector2(transform.position.x +5, transform.position.y);
        spellRB = GetComponent<Rigidbody2D>();
        Destroy(gameObject, 1f);
    }
    public void fireDirection(int dir)
    {
        if (dir == 1)
        {
            target = new Vector2(transform.position.x + 5, transform.position.y);
        }
        if (dir == 2)
        {
            target = new Vector2(transform.position.x - 5, transform.position.y);
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            Destroy(gameObject);
        }
        if (other.CompareTag("Obsticle"))
        {
            Destroy(gameObject);
        }
    }


    // Update is called once per frame
    void Update()
    {
        //transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
        spellRB.AddForce(transform.right * .50f, ForceMode2D.Impulse);
    }
}
