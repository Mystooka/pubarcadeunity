﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    private bool isRight;
    public GameObject explosion;
    public AudioClip hit;
    public AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        audio.clip = hit;

        // Physics.IgnoreCollision(this.collider, transform.root.collider);

        if (transform.position.x > 3)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.left * 200);
            transform.Rotate(0, 0, -90);
            isRight = true;
        }
        if (transform.position.x < 3)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * 200);
            transform.Rotate(0, 0, 90);
            isRight = false;

        }

    }
    void OnCollisionEnter2D(Collision2D other)
    {
        audio.Play();
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject, .2f);


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
