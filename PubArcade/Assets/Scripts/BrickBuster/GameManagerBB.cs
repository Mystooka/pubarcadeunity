﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerBB : MonoBehaviour
{
    public PaddleBB paddle;
    public BBall ball;
   /* public ScoreBB score;*/
    public static Vector2 btmLeft;
    public static Vector2 topRight;
    public static bool isPaused = false;
    private int leftBricks;
    private int rightBricks;
    private GameObject rpanel;
    private GameObject lpanel;
    private GameObject txtRight;
    private GameObject txtLeft;
    void Awake()
    {
        lpanel = GameObject.FindWithTag("rPanel");
        txtRight = GameObject.FindWithTag("rPlayerText");
        rpanel = GameObject.FindWithTag("lPanel");
        txtLeft = GameObject.FindWithTag("lPlayerText");

        btmLeft = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        topRight = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

        PaddleBB p1 = Instantiate(paddle) as PaddleBB;
        PaddleBB p2 = Instantiate(paddle) as PaddleBB;
        p1.Init(true);
        p2.Init(false);


        BBall b1 = Instantiate(ball) as BBall;
        BBall b2 = Instantiate(ball) as BBall;
        b1.Init(true);
        b2.Init(false);
    }
    void Start()
    {
    
    }

    void pauseGame()
    {

            Time.timeScale = 0f;
            isPaused = true;
        

    }
    void resumeGame()
    {

        Time.timeScale = 1f;
        isPaused = false;


    }
    public void SetRightBricks()
    {
        rightBricks++;
       // Debug.Log(rightBricks);
    }
    public void ReduceRightBricks()
    {
        rightBricks--;
       Debug.Log(rightBricks);
    }

    public void SetLeftBricks()
    {
        leftBricks++;
    }
    public void ReduceLeftBricks()
    {
        leftBricks--;
        Debug.Log(leftBricks);
    }

    // Update is called once per frame
    void Update()
    {
        if (rightBricks <= 0) {
            rpanel.SetActive(true);
            txtRight.GetComponent<Text>().text = "You Win!";
            Time.timeScale = 0f;
            GameObject.FindWithTag("Menu").SetActive(true);
        }
        if (leftBricks <= 0) {
            lpanel.SetActive(true);
            txtLeft.GetComponent<Text>().text = "You Win!";
            Time.timeScale = 0f;
            GameObject.FindWithTag("Menu").SetActive(true);
        }
    }
}
