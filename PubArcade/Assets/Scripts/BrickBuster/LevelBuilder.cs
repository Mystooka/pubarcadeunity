﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
    string[] lvlSet;
    // Start is called before the first frame update
    public TextAsset lvl1data;
    public TextAsset lvl2data;
    public TextAsset lvl3data;
    public TextAsset lvl4data;
    public TextAsset lvl5data;
    public TextAsset lvl6data;
    public TextAsset lvl7data;
    public TextAsset lvl8data;
    public TextAsset lvl9data;
    public TextAsset lvl10data;
    public TextAsset lvl11data;
    public TextAsset lvl12data;
    public TextAsset lvl13data;
    public TextAsset lvl14data;
    public TextAsset lvl15data;
    public TextAsset lvl16data;
    public TextAsset lvl17data;
    static int currentLVL =1;
    public string[] rows;
    private int cnt = 0;
    public bool readyChk;
    static int level;
    //static string[] shareRow;
    private string[] data;
    public void IncLevel()
    {
        level += 1;
    }
    void Awake()
    {

        if (level == 0)
        {
            
            level = 1;

        }

        switch (level)
        {
            case 1:
                data = lvl1data.text.Split(new char[] { '\n' });               
                break;
            case 2:
                data = lvl2data.text.Split(new char[] { '\n' });
                
                break;
            case 3:
                data = lvl3data.text.Split(new char[] { '\n' });
                
                break;
            case 4:
                data = lvl4data.text.Split(new char[] { '\n' });
                
                break;
            case 5:
                data = lvl5data.text.Split(new char[] { '\n' });             
                break;
            case 6:
                data = lvl6data.text.Split(new char[] { '\n' });
                break;
            case 7:
                data = lvl7data.text.Split(new char[] { '\n' });
                break;
            case 8:
                data = lvl8data.text.Split(new char[] { '\n' });

                break;
            case 9:
                data = lvl9data.text.Split(new char[] { '\n' });

                break;
            case 10:
                data = lvl10data.text.Split(new char[] { '\n' });

                break;
            case 11:
                data = lvl11data.text.Split(new char[] { '\n' });
                break;
            case 12:
                data = lvl2data.text.Split(new char[] { '\n' });
                break;
            case 13:
                data = lvl3data.text.Split(new char[] { '\n' });
                break;
            case 14:
                data = lvl14data.text.Split(new char[] { '\n' });

                break;
            case 15:
                data = lvl15data.text.Split(new char[] { '\n' });

                break;
            case 16:
                data = lvl16data.text.Split(new char[] { '\n' });

                break;
            case 17:
                data = lvl17data.text.Split(new char[] { '\n' });
                break;
            default:
                data = lvl1data.text.Split(new char[] { '\n' });
                level = 1;
                break;
        }

        

        for (int i = 0; i < data.Length - 1; i++)
        {
            string[] row = data[i].Split(new char[] { ',' });

            for (int j = 0; j < row.Length; j++)
            {
                rows[cnt] = row[j];
                cnt++;

            }

        }

        Debug.Log("Awake");


    }
    void Start()
    {
        //shareRow = rows;

    }


    // Update is called once per frame
    void Update()
    {

    }
}
