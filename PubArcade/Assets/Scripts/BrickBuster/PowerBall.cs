﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerBall : MonoBehaviour
{
    Rigidbody2D rb;
    private int life;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        int dir = Random.Range(1, 4);
        life = 4;
        switch (dir)
        {
            case 1:
                rb.AddForce(Vector2.right * 500);
                break;
            case 2:
                rb.AddForce(Vector2.left * 500);
                break;
            case 3:
                rb.AddForce(Vector2.down * 500);
                break;
            case 4:
                rb.AddForce(Vector2.up * 500);
                break;

        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (life == 0)
        {
            Destroy(this.gameObject);
        }

        Destroy(gameObject, 2f);
        
    }

    void OnCollisionEnter2D(Collision2D other)
    {
/*        if (other.transform.CompareTag("Walls"))
        {
            Destroy(this.gameObject);
        }*/
        if (other.transform.CompareTag("Brick"))
        {
            life--;
        }
            

    }

}
