﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public static bool inMenu = false;
    // Start is called before the first frame update
    private GameObject menu;
    private GameObject nxLvl;
  
    
    void Awake()
    {
        Time.timeScale = 1f;
        menu = GameObject.FindWithTag("Menu");
        nxLvl = GameObject.FindWithTag("LevelBuilder");
    }


    void Start()
    {

        menu.SetActive(false);
    }

    public void Pause()
    {
        menu.SetActive(true);
        inMenu = true;
        Time.timeScale = 0f;
    }
    public void NextLevel()
    {
        

        GameObject.FindWithTag("LevelBuilder").GetComponent<LevelBuilder>().IncLevel();
        //Application.LoadLevel(Application.loadedLevel);
        SceneManager.LoadScene("BrickBuster");
        //nxLvl.GetComponent<LevelBuilder>().IncLevel();
    }

    public void Resume()
    {


        menu.SetActive(false);
        Time.timeScale = 1f;
        inMenu = false;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Main");
        //Application.loadedLevelName(MainArcase);

        Time.timeScale = 0f;
    }

    // Update is called once per frame
}
