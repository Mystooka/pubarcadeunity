﻿
using UnityEngine;
using UnityEngine.UI;

public class BBall : MonoBehaviour
{
    private Rigidbody2D rb;
    private int bLife = 4;
    private bool playerLeft = true;
    private bool isPaused = false;
    public Sprite ball;
    public Sprite sOne;
    public Sprite sTwo;
    public Sprite sThree;
    private float awakeTimer = 0f;
    public Transform explosion;
    private GameObject h1;
    private GameObject h2;
    private GameObject h3;
    private GameObject h4;
    //private GameObject panRight;
    private GameObject panel;
    //private GameObject txtRight;
    private GameObject txtObj;
    private Text text;
    //private Text tR;

    private bool first = true;

    // Start is called before the first frame update
    private Vector2 size;
    public void Init(bool left)
    {

        size = this.transform.localScale;
        rb = GetComponent<Rigidbody2D>();
        if (left)
        {

            this.transform.tag = "BallLeft";
            playerLeft = true;
            transform.position = new Vector2(-7.5f, 0); 
            rb.AddForce(Vector2.right * 500);
            panel = GameObject.FindWithTag("lPanel") ;
            txtObj = GameObject.FindWithTag("lPlayerText");
            //text = txtLeft.GetComponent<Text>();
            panel.SetActive(false);
            h1 = GameObject.FindWithTag("lHeart1");
            h2 = GameObject.FindWithTag("lHeart2");
            h3 = GameObject.FindWithTag("lHeart3");
            h4 = GameObject.FindWithTag("lHeart4");
        }
        if (!left)
        {

            this.transform.tag = "BallRight";
            playerLeft = false;
            transform.position = new Vector2(7.5f, 0);
            rb.AddForce(Vector2.left * 500);
            panel = GameObject.FindWithTag("rPanel");
            txtObj = GameObject.FindWithTag("rPlayerText");
            //text = txtRight.GetComponent<Text>();
            panel.SetActive(false);
            h1 = GameObject.FindWithTag("rHeart1");
            h2 = GameObject.FindWithTag("rHeart2");
            h3 = GameObject.FindWithTag("rHeart3");
            h4 = GameObject.FindWithTag("rHeart4");
        }

    }
    void OnCollisionEnter2D(Collision2D other)
    {

        if (other.transform.CompareTag("Walls"))
        {
            bLife -= 1;
            Instantiate(explosion, this.transform.position, this.transform.rotation);
            switch (bLife)
            {
                case 0:
                    h1.SetActive(false);
                    break;
                case 1:
                    h2.SetActive(false);
                    break;
                case 2:
                    h3.SetActive(false);
                    break;
                case 3:
                    h4.SetActive(false);
                    break;


            }

            if (bLife <= 0)
            {
                Instantiate(explosion, this.transform.position, this.transform.rotation);
                
                panel.SetActive(true);
                txtObj.GetComponent<Text>().text = "You Lost";
                Destroy(this.gameObject);

            }

            if (playerLeft && bLife >= 1)
            {

                transform.position = new Vector2(-7.5f, 0);
                isPaused = true;
                if (first)
                {
                    this.transform.Rotate(0, 0, -90);
                    first = false;
                }

                rb.Sleep();
            }
            if (!playerLeft && bLife >= 1)
            {
                transform.position = new Vector2(7.5f, 0);
                if (first)
                {
                    this.transform.Rotate(0, 0, 90);
                    first = false;
                }
                rb.Sleep();


                isPaused = true;
              
            }
            


        }

    }

    public bool PlayerLeft()
    {
        return playerLeft;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPaused && bLife > 0)
        {
            awakeTimer = 4f;

        }


        if (awakeTimer > 0)
        {
            
            rb.WakeUp();
            awakeTimer -= Time.deltaTime;

            isPaused = false;
            if (awakeTimer <= 0f)
            {
                this.GetComponent<SpriteRenderer>().sprite = ball;
                this.transform.localScale = size;
                if (playerLeft)
                {
                    rb.AddForce(Vector2.right * 500);

                }
                else
                {
                    rb.AddForce(Vector2.left * 500);
                }
                isPaused = false;
                awakeTimer = 0;
            }
            else if (awakeTimer <= 1f)
            {
                

                this.GetComponent<SpriteRenderer>().sprite = sOne;
                this.transform.localScale = new Vector2(1.5f, 1.5f);

            }
            else if(awakeTimer <= 2f)
            {
                this.GetComponent<SpriteRenderer>().sprite = sTwo;
                this.transform.localScale = new Vector2(1.8f, 1.8f);
            }
            else if(awakeTimer <= 3f)
            {
                this.GetComponent<SpriteRenderer>().sprite = sThree;
                this.transform.localScale = new Vector2(2, 2);
            }

        }
    }


}
