﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleBB : MonoBehaviour
{
    Vector2 pos = Vector2.zero;
    public bool player1;
    private SpriteRenderer sR;
    // Start is called before the first frame update
    void Start()
    {
        sR = this.GetComponent<SpriteRenderer>();
        if (player1)
        {
            sR.color = Color.red;
        }
        else
        {
            sR.color = Color.cyan;
        }

    }
    public void Init(bool isPlayer1)
    {

        if (isPlayer1)
        {
            player1 = true;

            pos = new Vector2(8.5f, 0);
        }
        else
        {
            player1 = false;

            pos = new Vector2(-8.5f, 0);


        }
        //this.transform.SetPositionAndRotation(pos);
        this.transform.position = pos;
    }
    // Update is called once per frame
    void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            //Vector2 pos = Camera.main.ScreenToWorldPoint(touch);
            Vector2 tPos = Camera.main.ScreenToWorldPoint(touch.position);


            if (tPos.x < -3 && !player1)
            {
                //working
                transform.position = new Vector2(pos.x, tPos.y);

                //testing 

            }
            if (tPos.x > 3 && player1)
            {


                //working
                transform.position = new Vector2(pos.x, tPos.y);


            }


        }

    }
}
