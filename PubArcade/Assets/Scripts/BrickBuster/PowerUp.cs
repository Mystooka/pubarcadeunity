﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    private Rigidbody2D rb;
    public Sprite lockbonus;
    public Sprite Balla;
    public Sprite scrBalla;
    public Sprite lifeBalla;
    private int whatBonus;
    public PowerBall pb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        whatBonus = Random.Range(1, 3);

        switch (whatBonus)
        {
            case 1:
                this.GetComponent<SpriteRenderer>().sprite = lockbonus;
                break;
            case 2:
                this.GetComponent<SpriteRenderer>().sprite = Balla;
                break;
            case 3:
                this.GetComponent<SpriteRenderer>().sprite = scrBalla;
                break;
            case 4:
                this.GetComponent<SpriteRenderer>().sprite = lifeBalla;
                break;
        }

        if(this.transform.position.x < 0)
        {
           this.transform.Rotate(0,0,0);
            rb.AddForce(Vector2.left * 200);
        }
        if (this.transform.position.x > 0)
        {
            this.transform.Rotate(0, 0, 0);
            rb.AddForce(Vector2.right * 200);
        }

    }
    void OnCollisionEnter2D(Collision2D other)
    {


        if (this.transform.position.x < 0)
        {
            switch (whatBonus)
            {
                case 1:
                    foreach (GameObject brick in GameObject.FindGameObjectsWithTag("Brick"))
                    {
                        if (brick.transform.position.x > 0)
                        {

                            brick.GetComponent<Brick>().PowerLock(true);

                        }
                    }
                    break;
                case 2:
                    //new Vector2(-7.5f, 0)
                    //Vector2 ballpos = new Vector2(-7.5f, 0);
                    Vector3 ballpos = GameObject.FindGameObjectWithTag("BallLeft").transform.position;
                    Instantiate(pb, ballpos, Quaternion.identity);
                    Instantiate(pb, ballpos, Quaternion.identity);
                    Instantiate(pb, ballpos, Quaternion.identity);
                    break;
                case 3:
                    GameObject.FindWithTag("Score2").GetComponent<ScoreBB2>().IncScore2(50);
                    break;

            }

            Destroy(this.gameObject);
        }

        else if (this.transform.position.x > 0)
        {
            switch (whatBonus)
            {
                case 1:
                    foreach (GameObject brick in GameObject.FindGameObjectsWithTag("Brick"))
                    {
                        if (brick.transform.position.x < 0)
                        {

                            brick.GetComponent<Brick>().PowerLock(false);

                        }
                    }
                    break;
                case 2:
                    Vector3 ballpos = GameObject.FindGameObjectWithTag("BallRight").transform.position;
                    Instantiate(pb, ballpos, Quaternion.identity);
                    Instantiate(pb, ballpos, Quaternion.identity);
                    Instantiate(pb, ballpos, Quaternion.identity);
                   

                    break;
                case 3:
                    GameObject.FindWithTag("Score1").GetComponent<ScoreBB>().IncScore1(50);
                    break;

            }

            
            Destroy(this.gameObject);
        }

        if (other.transform.CompareTag("Walls"))
        {
            Destroy(this.gameObject);
        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
