﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleBB1 : MonoBehaviour
{
    Vector2 pos = Vector2.zero;
    public bool player1;
    private SpriteRenderer sR;
    public Sprite p1;
    public Sprite p2;
    private float xpos;
    public GameObject exp;

    //public Transform bulletTrans;
    // Start is called before the first frame update
    void Start()
    {
        //sR = this.GetComponent<SpriteRenderer>().sprite;
        if (player1)
        {
            this.GetComponent<SpriteRenderer>().sprite = p1;
            this.transform.Rotate(new Vector3(0, 0, 180));
            //sR.sprite = p1;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = p2;

        }

    }

    public void Destroy()
    {
        Instantiate(exp,transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
    public void Init(bool isPlayer1)
    {

        if (isPlayer1)
        {
            player1 = true;
            pos = new Vector2(GameManagerSpc.topRight.x -2f, 0);
            pos -= Vector2.right * transform.localScale.x;
            
            
            //pos = new Vector2(8.5f, 0);
        }
        else
        {
            player1 = false;
            pos = new Vector2(GameManagerSpc.btmLeft.x +2f, 0);
            
            pos += Vector2.right * transform.localScale.x;
            // pos = new Vector2(-8.5f, 0);


        }
        this.transform.position = pos;
        xpos = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {


        foreach (Touch touch in Input.touches)
        {
            //Vector2 pos = Camera.main.ScreenToWorldPoint(touch);
            Vector2 tPos = Camera.main.ScreenToWorldPoint(touch.position);
            //tPos.x = xpos;

            if (tPos.x < -3 && !player1)
            {
                Vector2 targetpos = new Vector2(xpos, tPos.y);
                //working
                transform.position = Vector2.MoveTowards(transform.position, targetpos, 6 * Time.deltaTime);

                //testing 

            }
            if (tPos.x > 3 && player1)
            {

                Vector2 targetpos = new Vector2(xpos, tPos.y);
                //working
                transform.position = Vector2.MoveTowards(transform.position, targetpos, 6 * Time.deltaTime);


            }


        }

    }
}
