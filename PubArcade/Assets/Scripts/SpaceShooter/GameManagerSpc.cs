﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerSpc : MonoBehaviour
{
    public PaddleBB1 paddle;

    public static Vector2 btmLeft;
    public static Vector2 topRight;
    public static bool isPaused = false;

    void Awake()
    {
        btmLeft = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        topRight = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));


    }
    void Start()
    {
        PaddleBB1 p1 = Instantiate(paddle) as PaddleBB1;
        PaddleBB1 p2 = Instantiate(paddle) as PaddleBB1;
        p1.Init(true);
        p2.Init(false);
    }

    void pauseGame()
    {

            Time.timeScale = 0f;
            isPaused = true;
        

    }
    void resumeGame()
    {

        Time.timeScale = 1f;
        isPaused = false;


    }


    // Update is called once per frame
    void Update()
    {

    }
}
