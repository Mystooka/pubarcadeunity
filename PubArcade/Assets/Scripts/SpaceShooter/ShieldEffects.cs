﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldEffects : MonoBehaviour
{

    int life = 3;
    private float fadeTimer = 0f;

    private bool isRight = true;
    public AudioClip hit;
    public AudioSource audioSrc;
    private GameObject menuCnt;
    void Awake()
    {
        audioSrc.clip = hit;
        menuCnt = GameObject.FindWithTag("MenuControls");

    }


    // Update is called once per frame
    void Update()
    {
        
        if (fadeTimer > 0)
        {
            fadeTimer -= Time.deltaTime;
            
            GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, fadeTimer);
            
          

        }
        if (life < 0)
        {
            
            transform.parent.gameObject.GetComponent<PaddleBB1>().Destroy();

            if (transform.position.x < 0)
            {
                menuCnt.GetComponent<MenuControls>().PlayerWin(true);

            }
            if (transform.position.x > 0)
            {
                menuCnt.GetComponent<MenuControls>().PlayerWin(false);

            }

            menuCnt.GetComponent<MenuControls>().SetVisable();

 
            
            Destroy(gameObject, .25f);
        }
        
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag("Comet") && life >= 0)
        {
            audioSrc.Play();
            
            transform.GetChild(life).GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, .2f);
            life--;
            fadeTimer = .8f;
        }
        if (other.transform.CompareTag("missle") && life >= 0)
        {
            audioSrc.Play();
            
            transform.GetChild(life).GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, .2f);
            life--;
            fadeTimer = .8f;
        }

    }

}
