﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootReady : MonoBehaviour
{
    public GameObject missle;
    private int missles = 2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag("FireTrigger"))
        {
            transform.GetChild(missles-1).GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            Instantiate(missle, transform.position, Quaternion.identity);
        }

    }
}
