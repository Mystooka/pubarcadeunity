﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class comet : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Destroy(gameObject, 3f);
        if (transform.position.x > 0)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * 200);
        }
        if (transform.position.x < 0)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.left * 200);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        Destroy(gameObject);
    }
}
