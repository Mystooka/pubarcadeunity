﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Physics2D.IgnoreCollision(transform.parent.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
    }

    // Update is called once per frame
    void Update()
    {
        {
            foreach (Touch touch in Input.touches)
            {
                //Vector2 pos = Camera.main.ScreenToWorldPoint(touch);
                Vector2 tPos = Camera.main.ScreenToWorldPoint(touch.position);


                if (tPos.x < -3)
                {

                    transform.position = new Vector2(tPos.x, transform.position.y);

                }
                if (tPos.x > 3)
                {

                    transform.position = new Vector2(tPos.x, transform.position.y);

                }


            }

        }
    }
}
