﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CometSpawner : MonoBehaviour
{
    public GameObject comet;
    private float spawnTimer = 2f;
    private int level = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0)
        {
            if (Random.Range(0, level) == 1)
            {
                Instantiate(comet, transform.position, Quaternion.identity);

            }
            else
            {
                level--;
                if (level <= 0)
                {
                    level = 8;
                }
            }

            spawnTimer = 2f;
        }

    }
}

