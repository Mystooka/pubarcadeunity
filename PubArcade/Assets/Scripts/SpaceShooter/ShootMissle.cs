﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootMissle : MonoBehaviour
{
    private bool isRight;
    private bool ready;
    private float shooTimer = 1f;
    public GameObject bullet;
    public AudioClip shot;
    public AudioSource audioSrc;
    // Start is called before the first frame update
    void Start()
    {
        audioSrc.clip = shot;
    }

    // Update is called once per frame
    void Update()
    {
        shooTimer -= Time.deltaTime;

        if (shooTimer <= 0)
        {
            audioSrc.Play();
            Instantiate(bullet, transform.position, Quaternion.identity);
            shooTimer = 1f;
        }


    }
}
