﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    
    Vector2 pos = Vector2.zero;
    public bool player1;
    private Rigidbody2D rb;
    private SpriteRenderer sR;
    //this.GetComponent<SpriteRenderer>().color = 0,0,2;
    void Start()
    {

        sR = this.GetComponent<SpriteRenderer>();
        if (player1)
        {
            sR.color = Color.cyan;
        }
        else
        {
            sR.color = Color.green;
        }
    }

    public void Init(bool isPlayer1)
    {
        //Vector2 pos = Vector2.zero;

        //TESTING NEW MOVE
        rb = GetComponent<Rigidbody2D>();
        /////////

        //place player 1 & 2 
        //Player1 right side
        if (isPlayer1)
        {
            player1 = true;
            pos = new Vector2(GameManager.topRight.x -1.5f, 0);
            pos -= Vector2.right * transform.localScale.x;
            
        }
        else
        {
            player1 = false;
            pos = new Vector2(GameManager.btmLeft.x + 1.5f, 0);
            pos += Vector2.right * transform.localScale.x;
        }

        transform.position = pos;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            //Vector2 pos = Camera.main.ScreenToWorldPoint(touch);
            Vector2 tPos = Camera.main.ScreenToWorldPoint(touch.position);

            
            if (tPos.x < -3 && !player1)
            {
                //working
                transform.position = new Vector2(pos.x, tPos.y);
                
                //testing 

            }
            if (tPos.x > -3 && player1)
            {


                //working
                transform.position = new Vector2(pos.x, tPos.y);
                

            }


        }

    }
}
