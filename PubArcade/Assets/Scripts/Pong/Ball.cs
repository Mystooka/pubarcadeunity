﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField]
    float speed;

    float dif = 5f;
    private bool rightRaused = false;

    public Transform rightScd;
    public Transform leftScd;

    float radius;
    Vector2 direction;
    private GameObject mng;
    // Start is called before the first frame update
    void Start()
    {
        direction -= Vector2.one.normalized;
        radius = transform.localScale.x / 2;
        mng = GameObject.FindGameObjectWithTag("PongManager");

    }

    // Update is called once per frame
    void Update()
    {


            transform.Translate(direction * (speed + dif) * Time.deltaTime);

            if (transform.position.y < GameManager.btmLeft.y + radius && direction.y < 0)
            {
                direction.y = -direction.y;
            }
            if (transform.position.y > GameManager.topRight.y - radius && direction.y > 0)
            {
                direction.y = -direction.y;
            }

            //Player scores

            if (transform.position.x < GameManager.btmLeft.x + radius && direction.x < 0)
            {
                direction.y = -direction.y;

            }
            if (transform.position.x > GameManager.topRight.x - radius && direction.x > 0)
            {
                direction.y = -direction.y;

            }

        //}



    }

    //the other variable send in what object collide with this one
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "LeftScore")
        {
            mng.GetComponent<GameManager>().PlayerScored(true);

            direction = Vector2.one.normalized;
            Instantiate(rightScd, this.transform.position, this.transform.rotation);

        }
        else if (other.tag == "RightScore")
        {
            //PlayerScored(false);
            mng.GetComponent<GameManager>().PlayerScored(false);
            direction = -Vector2.one.normalized;
            Instantiate(leftScd, this.transform.position, this.transform.rotation);

        }

        else if (other.tag == "Player" && other.tag != "RightScore" && other.tag != "LeftScore")
        {
            bool isPly1 = other.GetComponent<Player>().player1;
            if (isPly1 == true && direction.x > 0)
            {
                direction.x = -direction.x;
                
                dif += .25f;
                

            }
            if (isPly1 == false && direction.x < 0)
            {
                direction.x = -direction.x;
                
                dif += .25f;
                
            }
        }

    }
}
