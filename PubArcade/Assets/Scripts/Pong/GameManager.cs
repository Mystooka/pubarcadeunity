﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Ball ball;

    public Player player;
    public Score score;
     

    public static Vector2 btmLeft;
    public static Vector2 topRight;
    
    
    static Score s1;
    static Score s2;
    private int scr1 = 0;
    private int scr2 = 0;
    public GameObject menu;
    //static Ball b2;
    public GameObject leftWin;
    public GameObject rightWin;


    private Vector3 bpos = new Vector3(0f, 0f, 0f);
    // Start is called before the first frame update
    void Start()
    {
        //rightWin = GameObject.FindWithTag("MenuWinnerRight");
        //leftWin = GameObject.FindWithTag("MenuWinnerLeft");
        // menu = GameObject.FindWithTag("PongMenu");
        //get screen size for positioning 
        btmLeft = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        topRight = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

        //Create ball and player objects
        Instantiate(ball);
        Player p1 = Instantiate(player) as Player;
        Player p2 = Instantiate(player) as Player;
        
        p1.Init(true);
        p2.Init(false);

        s1 = Instantiate(score) as Score;
        s2 = Instantiate(score) as Score;

        s1.ScoreInit(true);
        s2.ScoreInit(false);

        //b2 = Instantiate(ball) as Ball;
        


    }
    public void PlayerScored(bool isScore1)
    {
        
        if (isScore1)
        {
            scr1++;

            if (scr1 >= 8)
            {
                rightWin.SetActive(true);

            }
            s1.IncScore();


        }
        else
        {
            scr2++;

            if (scr2 >= 8)
            {
                
                leftWin.SetActive(true);

            }
            s2.IncScore();

        }


    }

    // Update is called once per frame
    void Update()
    {
        if (scr1 >= 7 || scr2 >= 7)
        {
            menu.SetActive(true);
            Time.timeScale = 0f;

        }

    }
}
