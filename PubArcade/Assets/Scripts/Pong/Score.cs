﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Score : MonoBehaviour
{
    public bool player1;
    Vector2 pos = Vector2.zero;
    //private Rigidbody2D srb;
    // Start is called before the first frame update
    public Sprite sOne;
    public Sprite sTwo;
    public Sprite sThree;
    public Sprite sFour;
    public Sprite sFive;
    public Sprite sSix;
    public Sprite sSeven;
    public Sprite sEight;
    public Sprite sNine;
    //var newSprite : Sprite: One; 
    private SpriteRenderer sR;
    public int curScore = 0;

   
    void Start()
    {
        sR = this.GetComponent<SpriteRenderer>();
        if (!player1)
        {
            sR.color = Color.green;
        }
    }

    public void IncScore()
    {
        curScore += 1;
        
        switch (curScore)
        {
            case 1:
                this.GetComponent<SpriteRenderer>().sprite = sOne;
                break;
            case 2:
                this.GetComponent<SpriteRenderer>().sprite = sTwo;
                break;
            case 3:
                this.GetComponent<SpriteRenderer>().sprite = sThree;
                break;
            case 4:
                this.GetComponent<SpriteRenderer>().sprite = sFour;
                break;
            case 5:
                this.GetComponent<SpriteRenderer>().sprite = sFive;
                break;
            case 6:
                this.GetComponent<SpriteRenderer>().sprite = sSix;
                break;

            case 7:
                this.GetComponent<SpriteRenderer>().sprite = sSeven;
                break;
            case 8:
                this.GetComponent<SpriteRenderer>().sprite = sEight;
                break;
            case 9:
                this.GetComponent<SpriteRenderer>().sprite = sNine;
                break;
            default:

                break;
        }
    }

    public void ScoreInit(bool isScore1)
    {
        //srb = GetComponent<Rigidbody2D>();

        if (isScore1)
        {
            player1 = true;

            //GameObjectTo
            pos = new Vector2(GameManager.topRight.x -.5f, GameManager.topRight.y - 1);
            pos -= Vector2.right * transform.localScale.x;

            this.transform.Rotate(0, 0, 90);
        }
        else
        {
            player1 = false;
            pos = new Vector2(GameManager.btmLeft.x +.5f, GameManager.btmLeft.y + 1);
            pos += Vector2.right * transform.localScale.x;
            this.transform.Rotate(0,0,-90);
        }

        transform.position = pos;
    }

    // Update is called once per frame
    void Update()
    {



    }
}
