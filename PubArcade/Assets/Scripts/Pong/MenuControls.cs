﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControls : MonoBehaviour
{
    //public static bool inMenu = false;
    private GameObject menu;
    private GameObject leftWin;
    private GameObject rightWin;
    string gameId = "1234567";
    bool testMode = true;

    void Awake()
    {
        Time.timeScale = 1f;
        menu = GameObject.FindWithTag("PongMenu");
        rightWin = GameObject.FindWithTag("MenuWinnerRight");
        leftWin = GameObject.FindWithTag("MenuWinnerLeft");

    }
    void Start()
    {
        menu.SetActive(false);

        rightWin.SetActive(false);
        leftWin.SetActive(false);
    }
    public void SetVisable()
    {
        menu.SetActive(true);
        Time.timeScale = 0f;
    }
    public void PlayerWin(bool isRight)
    {
        if (isRight)
        {
            rightWin.SetActive(true);
        }
        else
        {
            leftWin.SetActive(true);
        }
    }

    public void Pause()
    {
        menu.SetActive(true);
        //inMenu = true;
        Time.timeScale = 0f;
    }
    public void Restart()
    {
       // SceneManager.LoadScene("pong");
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }


    public void Resume()
    {


        menu.SetActive(false);
        Time.timeScale = 1f;
        //inMenu = false;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Main");
        //Application.loadedLevelName(MainArcase);

        Time.timeScale = 0f;
    }


}