﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour
{
    public Sprite bb;
    public Sprite png;
    private int gamIndx = 0;
    SpriteRenderer sr;
    public Button btn;
    public void Start() {
        //sr = GameObject.FindWithTag("PlayGame").GetComponent<SpriteRenderer>();
    }
    public void ChangeGame(bool isNext)
    {
        

        if (isNext) { gamIndx++; }
        if (isNext != true && gamIndx > 0) {
            
            gamIndx--;
        }

        Debug.Log(gamIndx);

        switch (gamIndx)
        {
            case 0:    
                GameObject.FindWithTag("GameTitle").GetComponent<Text>().text = "Brick Buster";
                break;
            case 1:
                GameObject.FindWithTag("GameTitle").GetComponent<Text>().text = "Pong";
                break;
            case 2:
                GameObject.FindWithTag("GameTitle").GetComponent<Text>().text = "Space Battles";
                break;
            case 3:
                GameObject.FindWithTag("GameTitle").GetComponent<Text>().text = "More Comeing!";  
                break;
            default:

                break;
        }

        if (gamIndx >= 3) {
            GameObject.FindWithTag("GameTitle").GetComponent<Text>().text = "Brick Buster";
            gamIndx = 0;
        }

    }
    public void OnClick()
    {
        switch (gamIndx)
        {
            case 0:
                SceneManager.LoadScene("BrickBuster");
                break;

            case 1:
                SceneManager.LoadScene("Pong");
                break;
            case 2:
                SceneManager.LoadScene("SpaceShooter");
                gamIndx = 0;
                break;
        }

    }
    public void LoadGame()
    {
        switch (gamIndx)
        {
            case 0:
                SceneManager.LoadScene("BrickBuster");
                break;

            case 1:
                SceneManager.LoadScene("Pong");
                break;
            case 2:
                SceneManager.LoadScene("SpaceShooter");
                gamIndx = 0;
                break;
        }
    }


    public void LoadBRick()
    {
        SceneManager.LoadScene("BrickBuster");
    }
    public void LoadPong()
    {
        SceneManager.LoadScene("Pong");
    }

}
